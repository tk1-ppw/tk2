# Tugas Kelompok PPW Tahap 2 - Kelompok B14

[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

## Daftar isi

* [Anggota Kelompok](#anggota-kelompok)
* [Heroku App](#heroku-app)
* [Persona](#persona)
* [Wireframe dan Prototype](#wireframe-dan-prototype)
* [Latar Belakang](#latar-belakang-dan-manfaat)
* [Fitur](#fitur)

## Anggota Kelompok & Pembagian Tugas

1. Patrick Alexander - 1906398710 -> Statistik
2. Linus Abhyasa Wicaksana - 1906398761 -> Assesment Test
3. Nadia Sabrina - 1906399606 -> Rapid Test Location
4. Teofanus Gary Setiawan - 1906400160 -> Home Page & authentication

## Heroku App

HerokuApp dapat diakses di link [ini][herokuapp]

## Persona

Persona untuk website kami dapat dilihat pada link [ini][persona]

## Wireframe dan Prototype

Wireframe dapat diakses pada link [ini][wireframe] dan prototype dapat diakses pada link [ini][prototype]

## Latar Belakang dan Manfaat

Kelompok kami ingin menyajikan serta menyebarkan informasi tentang kondisi COVID saat ini. Dengan website ini kelompok kami berharap agar masyarakat banyak lebih tersadar agar lebih berhati - hati dan menyadari berbahayanya virus Corona. Kami berharap agar website ini dapat memudahkan orang untuk mengakses informasi mengenai corona.  

## Fitur

* Dashboard yang berisi data dan statistik mengenai status korban COVID saat ini (secara nasional dan per provinsi)
* Pencarian lokasi rapid test
* Berita mengenai COVID 19 beserta tips dan trik untuk mencegah terjangkit corona
* Form assesment sederhana untuk mengecek risiko terjangkit COVID
* Pengguna dapat membuat akun untuk menyimpan hasil assesment testnya

> ⚠ **Assesment yang ada tidak memiliki akurasi 100%, segera hubungi atau pergi ke rumah sakit terdekat apabila mengalami gejala-gejala**

[pipeline-badge]: https://gitlab.com/tk1-ppw/tk2/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/tk1-ppw/tk2/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/tk1-ppw/tk2/-/commits/master
[herokuapp]: https://coronahub.herokuapp.com/
[persona]: https://docs.google.com/document/d/1wbaorFZySBiopvwLLtifUjl3ngRl6OOkC7JoPgas51Y/edit?usp=sharing
[wireframe]: https://www.figma.com/file/fy0yoe6nwUw2ICed6giX1v/Wireframe-TK?node-id=0%3A1
[prototype]: https://www.figma.com/file/mYjRskZdOj6UcMeXLjXuqT/Prototype-TK?node-id=0%3A1