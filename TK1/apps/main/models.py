from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.

class News(models.Model):
    title = models.CharField(max_length=150)
    link = models.URLField(max_length=256)
    image_headline = models.URLField(max_length=256)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'news'
        verbose_name_plural = 'news'

    def __str__(self) -> str:self.title

class Tips(models.Model):
    title = models.CharField(max_length=128)
    description = models.CharField(max_length=256)
    image = models.ImageField(null=True, blank=True, upload_to="tips_images/")
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'tip'
        verbose_name_plural = 'tips'

    def __str__(self) -> str:self.title
