from django.test import TestCase
from django.urls import reverse
from django.test import Client
from TK1.apps.main.models import Tips, News
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth import get_user_model
import requests

class UniTest(TestCase):
    def setUp(self):
        # Need to be logged-in first to access tips and news page
        self.client = Client()
        self.user = get_user_model().objects.create_user(username="TestName", password="testpassword")

    def test_home_path_page_exist(self):
        self.assertEqual(self.client.get(reverse('main:home')).status_code, 200)

    def test_tips_path_page_exist(self):
        self.client.login(username="TestName", password="testpassword")
        self.assertEqual(self.client.get(
            reverse('main:tips')).status_code, 302)

    def test_news_path_page_exist(self):
        self.client.login(username="TestName", password="testpassword")
        self.assertEqual(self.client.get(
            reverse('main:news')).status_code, 302)

    # Test if the correct template is used

    def test_home_template_is_used(self):
        self.assertTemplateUsed(self.client.get(
            reverse('main:home')), 'main/home.html')

    def test_tips_template_is_used(self):
        self.client.login(username="TestName", password="testpassword")
        self.assertTemplateUsed((self.client.get(
            reverse('main:tips')), 'main/tips.html'))
    
    def test_news_template_is_used(self):
        self.client.login(username="TestName", password="testpassword")
        self.assertTemplateUsed((self.client.get(
            reverse('main:news')), 'main/news.html'))

    # Test adding tips

    def test_add_tips(self):
        # Test for adding a successfull tips
        self.client.login(username="TestName", password="testpassword")
        image = open('TK1/static/images/default.png', 'rb')
        data = {
            'title': 'Title Test 1',
            'description': 'Description Test 1',
            'image': SimpleUploadedFile(image.name, image.read())
        }
        response = self.client.post(reverse('main:tips'), data, follow = True)
        self.assertEqual(Tips.objects.all().count(), 1)

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'Tips added successfully!')

        # Test if redirections works
        # self.assertRedirects(response, reverse('main:home'))

        # Test for adding a wrong tips
        image = open('TK1/static/images/logo.svg', 'rb')
        data = {
            'title': 'Title Test 1',
            'description': 'Description Test 1',
            'image': SimpleUploadedFile(image.name, image.read())
        }
        response = self.client.post(reverse('main:tips'), data, follow = True)
        self.assertEqual(Tips.objects.all().count(), 1) # Compared to one because of previous test that adds a tips

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[1]), 'The uploaded file must be an image!')

        # Test if redirections works
        self.assertRedirects(response, reverse('main:home'))

        # Test if request is GET
        response = self.client.get(reverse('main:tips'))
        self.assertEqual(response.status_code, 302)

    # Test removing tips

    def test_remove_tips(self):
        self.client.login(username="TestName", password="testpassword")
        image = open('TK1/static/images/default.png', 'rb')
        data = {
            'title': 'Title Test 1',
            'description': 'Description Test 1',
            'image': SimpleUploadedFile(image.name, image.read())
        }
        self.client.post(reverse('main:tips'), data, follow=True)
        tips = Tips.objects.get(title="Title Test 1")
        response = self.client.get(reverse('main:remove_tips', args=[tips.id]), follow = True)
        self.assertEqual(News.objects.all().count(), 0)

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[1]), 'Tips has been deleted')

        # Test if redirection works
        self.assertRedirects(response, reverse('main:home'))
    
    # Test adding news
    
    def test_add_news(self):
        # Test adding a correct news
        self.client.login(username="TestName", password="testpassword")
        response = self.client.post(reverse('main:news'), 
            {
                'title': 'Satgas Ungkap Sebab Testing Covid-19 Indonesia di Bawah WHO', 
                'link': 'https://www.cnnindonesia.com/nasional/20201118151530-20-571399/satgas-ungkap-sebab-testing-covid-19-indonesia-di-bawah-who', 
                'image_headline': 'https://akcdn.detik.net.id/visual/2020/10/03/swab-test-drive-thru-8_169.jpeg?w=650'
            }, follow = True)
        self.assertEqual(News.objects.all().count(), 1)

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'News added successfully!')

        # Test if redirections works
        # self.assertRedirects(response, reverse('main:home'))

        # Test when request is GET
        response = self.client.get(reverse('main:news'))
        self.assertEqual(response.status_code, 302)

    # Test removing news

    def test_remove_news(self):
        self.client.login(username="TestName", password="testpassword")
        response = self.client.post(reverse('main:news'), 
            {
                'title': 'Satgas Ungkap Sebab Testing Covid-19 Indonesia di Bawah WHO', 
                'link': 'https://www.cnnindonesia.com/nasional/20201118151530-20-571399/satgas-ungkap-sebab-testing-covid-19-indonesia-di-bawah-who', 
                'image_headline': 'https://akcdn.detik.net.id/visual/2020/10/03/swab-test-drive-thru-8_169.jpeg?w=650'
            }, follow = True)
        news = News.objects.get(
            title="Satgas Ungkap Sebab Testing Covid-19 Indonesia di Bawah WHO")
        response = self.client.get(reverse('main:remove_news', args=[news.id]), follow = True)
        self.assertEqual(News.objects.all().count(), 0)

        # Test correct message
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[1]), 'News has been deleted')

        # Test if redirection works
        self.assertRedirects(response, reverse('main:home'))

    def test_fetch_news(self):
        response = self.client.get(reverse('main:fetch_news'))
        url = "https://newsapi.org/v2/top-headlines?country=id&q=covid&apiKey=37086462a7524d0abaf7b8e2aa221bfa"
        response2 = requests.get(url).json()
        self.assertJSONEqual(str(response.content, encoding='utf8'), response2)

    def test_fetch_covid_status(self):
        response = self.client.get(reverse('main:fetch_covid_status'))
        url = "https://api.kawalcorona.com/indonesia"
        response2 = requests.get(url).json()[0]
        self.assertJSONEqual(str(response.content, encoding='utf8'), response2)
