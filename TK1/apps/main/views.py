from django.shortcuts import get_object_or_404, render, redirect
from .models import News, Tips
from .forms import NewsForm, TipsForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import requests

NEWS = News.objects.all()
TIPS = Tips.objects.all()

def home(request):
    news_form = NewsForm(request.POST or None)
    tips_form = TipsForm(request.POST or None, request.FILES or None)
    response = { 
        'news': News.objects.all(), 
        'tips': Tips.objects.all(), 
        'news_form': news_form,
        'tips_form': tips_form}
    return render(request, 'main/home.html', response)

@login_required
def news(request):
    form = NewsForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            news = form.save(commit = False)
            news.user = request.user
            news.save()
            messages.success(request, "News added successfully!")
        return render(request, 'main/news.html', {'news': News.objects.all()})

    return redirect('main:home')

@login_required
def tips(request):
    form = TipsForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            tips = form.save(commit = False)
            tips.user = request.user
            tips.save()
            messages.success(request, "Tips added successfully!")
            return render(request, 'main/tips.html', {'tips': Tips.objects.all()})
        else:
            messages.error(request, "The uploaded file must be an image!")
            return redirect('main:home')
    return redirect('main:home')

@login_required
def remove_tips(request, id):
    tips = get_object_or_404(Tips, pk=id)
    tips.delete()
    messages.success(request, "Tips has been deleted")
    return redirect('main:home')

@login_required
def remove_news(request, id):
    news = get_object_or_404(News, pk=id)
    news.delete()
    messages.success(request, "News has been deleted")
    return redirect('main:home')

def fetch_news(request):
    url = "https://newsapi.org/v2/top-headlines?country=id&q=covid&apiKey=37086462a7524d0abaf7b8e2aa221bfa"
    response = requests.get(url).json()

    return JsonResponse(response)

def fetch_covid_status(request):
    url = "https://api.kawalcorona.com/indonesia"
    response = requests.get(url).json()[0]

    return JsonResponse(response)
