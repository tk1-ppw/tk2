from django.forms import ModelForm
from .models import News, Tips

class NewsForm(ModelForm):
    class Meta:
        model = News
        exclude = ['user']

class TipsForm(ModelForm):
    class Meta:
        model = Tips
        exclude = ['user']
