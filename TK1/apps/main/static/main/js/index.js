function hasOneDayPassed(){
    var date = new Date().toLocaleDateString();

    if (localStorage.last_update == date)
        return false;

    localStorage.last_update = date;
    return true;
};

function slickified() {
    $("#news").slick({
        mobileFirst: true,
        autoplay: true,
        prevArrow: "<img class='a-left control-c prev slick-prev' src='/static/main/images/arrow-left.svg'>",
        nextArrow: "<img class='a-right control-c next slick-next' src='/static/main/images/arrow-right.svg'>",
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    mobileFirst: true,
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    mobileFirst: true,
                    slidesToShow: 1,
                }
            }
        ]
    })
};

$(function() {
    slickified();

    $(document).on("click", "#add-news", function() {
        console.log("Clicked news")
        $("#news-form").modal({
            fadeDuration: 250
        });
        return false;
    });

    $(document).on("click", "#add-tips", function() {
        console.log("Clicked tips")
        $("#tips-form").modal({
            fadeDuration: 250
        });
        return false;
    });

    $("#news-form form").submit(function(event) {
        event.preventDefault();
        let form_data = $(this).serialize();
        
        // Send the data with ajax back to Django for adding to model
        $.ajax({
            url: "news/",
            data: form_data,
            type: "POST",
            success: (data) => {
                console.log("Success adding news");

                $("#news-container").replaceWith(data);

                slickified();

                $(this).trigger("reset");
                $.modal.close();
            },
            failure: () => {
                console.log("Failure adding news");
            },
        });
    })

    $("#tips-form form").on("submit", function(event) {
        event.preventDefault();
        let form_data = new FormData(this);

        // Send the data with ajax back to Django for adding to model
        $.ajax({
            url: "tips/",
            data: form_data,
            processData: false,
            contentType: false,
            type: "POST",
            success: (data) => {
                console.log("Success adding tips");

                $("#tips-container").replaceWith(data);

                $(this).trigger("reset");
                $.modal.close();
            },
            failure: () => {
                console.log("Failure adding tips");
            },
        });
    })

    // AJAX for updating news (only update each day, dependes on user sadly)
    if (true || hasOneDayPassed()) {
        $.ajax({
            url: "/fetch-news",
            success: (data) => {
                console.log("Success on fetching news");
                $.each(data.articles, function (key, value) {
                    let title = value.title;
                    let link = value.url;
                    let image_headline = value.urlToImage;

                    let clone = $("#news-template").contents().clone(true);
                    clone.find("header").text(title);
                    clone.find("object").attr("data", image_headline);
                    clone.find("#url-link").attr("href", link);

                    $("#news").slick("slickAdd", clone);
                });
            },
            failure: () => {
                alert("There is a failure on fetching news");
            },
        });
    }

    // AJAX for updating COVID Statuses
    $.ajax({
        url: "/fetch-covid-status",
        success: (data) => {
            console.log("Success on fetching COVID status!");

            let positif = data.positif;
            let sembuh = data.sembuh;
            let meninggal = data.meninggal;

            $("#covid-status").find("#positif").text(positif);
            $("#covid-status").find("#sembuh").text(sembuh);
            $("#covid-status").find("#meninggal").text(meninggal);
        },
        failure: () => {
            alert("There is a failure on fetching COVID status");
        }
    });
})