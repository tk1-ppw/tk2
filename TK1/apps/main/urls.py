from django.urls import path
from .views import home, remove_tips, remove_news, news, tips, fetch_news, fetch_covid_status
from django.conf import settings
from django.conf.urls.static import static

app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('news/', news, name='news'),
    path('tips/', tips, name='tips'),
    path('remove-tips/<int:id>', remove_tips, name='remove_tips'),
    path('remove-news/<int:id>', remove_news, name='remove_news'),
    path('fetch-news', fetch_news, name='fetch_news'),
    path('fetch-covid-status', fetch_covid_status, name='fetch_covid_status')
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
