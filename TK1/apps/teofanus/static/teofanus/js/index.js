toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-top-full-width",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}

$(function () {
    $("#register-form form").submit(function (event) {
        event.preventDefault();
        let form_data = new FormData(this);

        // Send the data with ajax back to Django for adding to model
        $.ajax({
            url: "/accounts/register/",
            data: form_data,
            processData: false,
            contentType: false,
            type: "POST",
            success: (data) => {
                console.log("Success adding user");
                toastr.success("Account created successfully!");
                toastr.info("You are the " + ordinal_suffix_of(data.total_user) + " user")

                console.log(data.total_user);

                $(this).trigger("reset");
                window.location.href = "/accounts/login/";
            },
            failure: () => {
                console.log("Failure adding user");
            },
        });
    });

    $("img").on('click', function() {
        $(".image-clone").modal({
            fadeDuration: 250
        });
        return false;
    });
});