from django.urls import path

from .views import login, register, logout_account, profile

app_name = "teofanus"

urlpatterns = [
    path('login/', login, name="login"),
    path('register/', register, name="register"),
    path('logout_account', logout_account, name="logout_account"),
    path('profile', profile, name="profile"),
]
