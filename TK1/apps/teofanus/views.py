from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from .forms import RegisterForm
from django.contrib.auth.forms import  AuthenticationForm
from django.contrib import messages
from django.contrib.auth import authenticate, get_user, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model

# Create your views here.

def login(request):
    form = AuthenticationForm(request.POST or None)
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password)
        
        if user is not None:
            auth_login(request, user)
            messages.success(request, "You have logged in successfully!")
            return redirect('main:home')
        else:
            messages.error(
                request, "No account can be found, please check your credentials or register first if you have not done so")
            return redirect('teofanus:login')
    response = {'login_form': form}
    return render(request, 'teofanus/login.html', response)

def register(request):
    form = RegisterForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            # messages.success(request, "Account created successfully!")
            return JsonResponse({"total_user": len(get_user_model().objects.all())})
            # return redirect('teofanus:login')
    response = {'register_form': form}
    return render(request, 'teofanus/register.html', response)

@login_required
def logout_account(request):
    logout(request)
    return redirect('main:home')

@login_required
def profile(request):
    return render(request, 'teofanus/profile.html')
