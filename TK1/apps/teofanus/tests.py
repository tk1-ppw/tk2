from django.test import TestCase
from django.test import Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from .forms import RegisterForm
from django.contrib.auth import get_user_model

# Create your tests here.

class UnitTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up data for the whole TestCase
        cls.login_response = Client().get(reverse('teofanus:login'))
        cls.register_response = Client().get(reverse('teofanus:register'))

    # Test for login page

    def test_login_path_page_exist(self):
        self.assertEqual(self.login_response.status_code, 200)

    def test_register_path_page_exist(self):
        self.assertEqual(self.register_response.status_code, 200)

    def test_profile_path_page_exist(self):
        self.client = Client()
        self.user = get_user_model().objects.create_user(username="TestName", password="testpassword")
        self.client.login(username="TestName", password="testpassword")
        self.assertEqual(self.client.get(
            reverse('teofanus:profile')).status_code, 200)

    # Test if the correct template is used

    def test_login_template_is_used(self):
        self.assertTemplateUsed(self.login_response, 'teofanus/login.html')

    def test_register_template_is_used(self):
        self.assertTemplateUsed(self.register_response, 'teofanus/register.html')

    def test_profile_template_is_used(self):
        self.client = Client()
        self.user = get_user_model().objects.create_user(
            username="TestName", password="testpassword")
        self.client.login(username="TestName", password="testpassword")
        self.assertTemplateUsed(self.client.get(reverse('teofanus:profile')),
                                'teofanus/profile.html')

    # Test login and register account in models

    def test_login_account(self):
        # Prepare user account
        get_user_model().objects.create_user(username="TestName", password="testpassword")

        # Test successful login
        response = self.client.post(reverse('teofanus:login'), {'username': "TestName", 'password': "testpassword"}, follow=True)
        messages = list(response.context['messages'])
        self.assertEqual(str(messages[0]), 'You have logged in successfully!')
        self.assertTrue(response.context['user'].is_authenticated)

        # Test if redirections works
        self.assertRedirects(response, reverse('main:home'))

        # Test failed login because of wrong credentials
        response = self.client.post(reverse('teofanus:login'), {
                                    'username': "TestName2", 'password': "testpassword2"}, follow=True)
        messages = list(response.context['messages'])
        self.assertEqual(str(
            messages[0]), 'No account can be found, please check your credentials or register first if you have not done so')

        # Test if redirections works
        self.assertRedirects(response, reverse('teofanus:login'))

        # Test if the request is not in POST (in GET)
        response = self.client.get(reverse('teofanus:login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teofanus/login.html')

    def test_register_account(self):
        # Test successful account creation
        response = self.client.post(reverse('teofanus:register'), {
                                    'username': "TestUsername2", 'email': "test2@email.com", 'password1': "testpassword2", 'password2': "testpassword2", 'province': "Jambi"}, follow=True)
        # messages = list(response.context['messages'])
        # self.assertEqual(str(messages[0]), 'Account created successfully!')
        self.assertEqual(get_user_model().objects.all().count(), 1)

        # Test failed account creation because of already existing username
        form_data = {'username': "TestUsername2", 'email': "test3@email.com",
                     'password1': "testpassword2", 'password2': "testpassword2"}
        form = RegisterForm(data = form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('A user with that username already exists.', form.errors['username'])

        # Test failed account creation because of already existing email
        form_data = {'username': "TestUsername3", 'email': "test2@email.com",
                     'password1': "testpassword2", 'password2': "testpassword2"}
        form = RegisterForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('User with this Email already exists.', form.errors['email'])

        # Test failed account creation because of not matching password
        form_data = {'username': "TestUserame3", 'email': "test3@email.com",
                     'password1': "testpassword2", 'password2': "testpassword3"}
        form = RegisterForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('The two password fields didn’t match.', form.errors['password2'])

        # Test failed account creation because of username containing illegal character
        form_data = {'username': "[][]]", 'email': "test3@email.com",
                     'password1': "testpassword2", 'password2': "testpassword2"}
        form = RegisterForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('Enter a valid username. This value may contain only letters, numbers, and @/./+/-/_ characters.',
                      form.errors['username'])

        # Test if the request is not in POST (in GET)
        response = self.client.get(reverse('teofanus:register'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'teofanus/register.html')
    
    def test_logout(self):
        # Prepare user account
        get_user_model().objects.create_user(username="TestName", password="testpassword")

        # Login
        self.client.post(reverse('teofanus:login'), {
                         'username': "TestName", 'password': "testpassword"}, follow=True)

        response = self.client.get(reverse('teofanus:logout_account'))

        # Test if redirections works
        self.assertRedirects(response, reverse('main:home'))
        

    # Test if essential text and button exist in login and register page

    def test_important_text_in_login_page(self):
        self.assertContains(self.login_response, 'Masuk')
        self.assertContains(self.login_response, 'Username')
        self.assertContains(self.login_response, 'Password')

    def test_important_text_in_register_page(self):
        self.assertContains(self.register_response, 'Daftar')
        self.assertContains(self.register_response, 'Username')
        self.assertContains(self.register_response, 'Email')
        self.assertContains(self.register_response, 'Password')
