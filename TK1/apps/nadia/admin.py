from django.contrib import admin
from .models import KotaModel
from .models import TempatModel
from .models import CommentModel

# Register your models here.

admin.site.register(KotaModel)
admin.site.register(TempatModel)
admin.site.register(CommentModel)
