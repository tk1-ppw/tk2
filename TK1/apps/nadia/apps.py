from django.apps import AppConfig


class NadiaConfig(AppConfig):
    name = 'nadia'
    verbose_name = 'TK1.apps.nadia'
