from django.shortcuts import render, redirect,reverse
from django.http import JsonResponse
from TK1.apps.nadia.models import KotaModel
from TK1.apps.nadia.models import TempatModel
from TK1.apps.nadia.models import CommentModel

# Create your views here.

def lokasiTest(request):
    if not request.user.is_authenticated:
            return redirect('teofanus:login')
    tempats = TempatModel.objects.all()
    if request.method == 'POST':
        query = request.POST['lokasi']
        tempats = tempats.filter(alamat__icontains=request.user.province)
        
    context = {
        'tempats': tempats,
    }
    return render(request, "lokasiTest.html", context)

# fungsi halaman comment
def comment(request):
    comments = CommentModel.objects.all()
    response_data = {}
    if request.POST.get('action') == 'post':
        title = request.POST.get('title')
        description = request.POST.get('description')
        response_data['title'] = title
        response_data['description'] = description
        CommentModel.objects.create(
            title = title,
            description = description,
            )
        return JsonResponse(response_data)
        data = JsonResponse(response_data)
    return render(request, 'Comment.html', {'comments':comments}) 
