from django.test import TestCase, Client
from django.urls import reverse, resolve
from TK1.apps.nadia.models import *
from django.contrib.auth.models import User
from . import views
# Create your tests here.

class TestLokasiTest(TestCase):
    def test_apakah_url_lokasiTest_ada(self):
        response = self.client.get('/rapid-test/lokasiTest')
        self.assertEquals(response.status_code, 302)

    # def test_apakah_di_halaman_lokasiTest_ada_templatenya(self):
    #     response = self.client.get('/rapid-test/lokasiTest')
    #     self.assertTemplateUsed(response, 'lokasiTest.html')

    def test_KotaModel(self):
        KotaModel.objects.create(nama_kota='kota')
        kota = KotaModel.objects.get(nama_kota='kota')
        self.assertEqual(str(kota), 'kota')

    def test_TempatModel(self):
        kota = KotaModel.objects.create(nama_kota='kota')
        TempatModel.objects.create(nama_tempat='tempat', alamat="alamat", gmaps="test", deskripsi="deskripsi", lokasi= kota)
        tempat = TempatModel.objects.get(nama_tempat='tempat')
        self.assertEqual(str(tempat), '1.tempat')

    def test_if_user_not_authenticated(self):
        request = self.client.get('/rapid-test/lokasiTest')
        self.assertRedirects(request, reverse('teofanus:login'))

    def test_apakah_url_comment_ada(self):
        response = self.client.get('/rapid-test/comment')
        self.assertEquals(response.status_code, 200)
    
    def test_apakah_di_halaman_comment_ada_templatenya(self):
        response = self.client.get('/rapid-test/comment')
        self.assertTemplateUsed(response, 'Comment.html')

    # def test_func_comment_page(self):
    #     found = resolve('/rapid-test/comment')
    #     self.assertEqual(found.func, views.comment)
    
    def test_CommentModel(self):
        CommentModel.objects.create(title='judul')
        title = CommentModel.objects.get(title='judul')
        self.assertEqual(str(title), 'judul')

    def test_menambahkan_komentar_dari_form(self):
        response = self.client.post('/rapid-test/comment', data={'title':'title', 'description' : 'deskripsi'})
        self.assertEqual(response.status_code, 200)
        html = response.content.decode('utf8')
        self.assertIn("title", html)

    def test_filter(self):
        response = self.client.post('/rapid-test/lokasiTest', data={'lokasi':'DKI Jakarta'})
        self.assertEqual(response.status_code, 302)

    


    




