# Generated by Django 3.1.4 on 2021-01-01 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nadia', '0004_auto_20201120_2333'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommentModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=50, null=True)),
                ('description', models.TextField(blank=True, null=True)),
            ],
        ),
    ]
