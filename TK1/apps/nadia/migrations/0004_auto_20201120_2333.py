# Generated by Django 3.1.3 on 2020-11-20 23:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nadia', '0003_tempatmodel_gmaps'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tempatmodel',
            old_name='kota',
            new_name='lokasi',
        ),
    ]
