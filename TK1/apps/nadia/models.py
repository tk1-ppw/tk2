from django.db import models

# Create your models here.
class KotaModel(models.Model):
    nama_kota = models.CharField(max_length=100)
    def __str__(self):
        return self.nama_kota

class TempatModel(models.Model):
    nama_tempat = models.CharField(max_length=100)
    alamat = models.CharField(max_length=300, blank=True, null=True)
    gmaps = models.CharField(max_length=500, blank=True, null=True)
    deskripsi = models.TextField(max_length = 800)
    lokasi = models.ForeignKey(KotaModel, on_delete=models.CASCADE)
    def __str__(self):
        return "{}.{}".format(self.id, self.nama_tempat)

class CommentModel(models.Model):
    title = models.CharField(max_length=50,null=True, blank=True)
    description = models.TextField(null=True,blank=True)
    def __str__(self):
        return self.title




