from django.contrib import admin
from django.urls import path, include

from . import views
app_name = 'TK1.apps.nadia'

urlpatterns = [
    path('lokasiTest', views.lokasiTest, name='lokasiTest' ), 
    path('comment', views.comment, name ='comment'),
]